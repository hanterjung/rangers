﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
	public float spawnTime = 3f;		// The amount of time between each spawn.
	public float spawnDelay = 3f;		// The amount of time before spawning starts.
	public GameObject[] enemies;		// Array of enemy prefabs.

	public int spawnNum = 3;
	public bool bRandomSpawnNum = false;

	public float limitTargetDist = 3f;
	public float halfMapWidth = 9f;
	public float halfMapHeight = 6.6f;

	private EnemyManager enemyManager;
	private Transform player;

	private Player gamePlayer;

	void Awake ()
	{
		enemyManager = GameObject.FindGameObjectWithTag ("Manager").GetComponent<EnemyManager>();
		player = GameObject.FindGameObjectWithTag("Player").transform;

		gamePlayer = Player.GetPlayer ();
	}


	void Start ()
	{
		// Start calling the Spawn function repeatedly after a delay .
		InvokeRepeating("Spawn", spawnDelay, spawnTime);
	}
	
	
	void Spawn ()
	{
		if (enemyManager.isFullEnemy () || gamePlayer.IsGameOver) {
			return;
		}
		print ("Spawn! " + enemyManager.EnemyCount);

		//move spawner position
		while(true) {
			Vector3 newPos = new Vector3 (Random.Range(-halfMapWidth, halfMapWidth),
			                              Random.Range(-halfMapHeight, halfMapHeight), 0);
			
			float dist = Vector3.Distance(newPos, player.position);
			if( dist > limitTargetDist ) {
				transform.position = newPos;
				break;
			}
		}

		// Instantiate a random enemy.
		int enemyIndex = Random.Range(0, enemies.Length);
		int rndSpawnNum = spawnNum;
		if(bRandomSpawnNum) rndSpawnNum = Random.Range(1, spawnNum+1);

		for (int i=0; i<rndSpawnNum; i++) {
			Vector3 v = new Vector3(Random.Range(0, 0.1f), Random.Range(0, 0.1f));
			Instantiate (enemies [enemyIndex], transform.position+v, transform.rotation);

			enemyManager.AddEnemy();
		}
		// Play the spawning effect from all of the particle systems.
		/*foreach(ParticleSystem p in GetComponentsInChildren<ParticleSystem>())
		{
			p.Play();
		}*/

	}
}
