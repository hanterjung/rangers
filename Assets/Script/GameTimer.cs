﻿using UnityEngine;
using System.Collections;

public class GameTimer : MonoBehaviour {
	private float startTime;
	private float time;
	private int minute, sec, milliSec;
	public Player player;

	void Awake() {
		instance = this;

	}

	// Use this for initialization
	void Start () {
		startTime = Time.realtimeSinceStartup;
	}
	
	// Update is called once per frame
	void Update () {
		if (player.IsGameOver) return;

		time = Time.realtimeSinceStartup - startTime;

		minute = (int)(time / 60);
		sec = (int)(time % 60);
		milliSec = (int)((time * 100) % 100);

		string text = "";

		if (minute < 10) {
			text += "0";
		}
		text += minute;

		text += ":";
		if (sec < 10) {
			text += "0";
		}
		text += sec;

		text += ":";
		if (milliSec < 10) {
			text += "0";
		}
		text += milliSec;

		guiText.text = text;
	}

	private static GameTimer instance = null;
	public static GameTimer SharedScore() {
		return instance;
	}
}
