﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public int point = 100;

	public bool bRangeEnemy = false;
	public float fireRate = 1f;
	public float missileVelocity = 10f;

	public float moveForce = 30f;			// Amount of force added to move the player left and right.
	//public float maxSpeed = 5f;				// The fastest the player can travel in the x axis.
	public float moveSpeed = 6f;		// The speed the enemy moves at.

	public bool bEnableRotation = false;
	public float rotateSpeed = 10f;
	public int HP = 1;					// How many times the enemy can be hit before it dies.
	//public Sprite deadEnemy;			// A sprite of the enemy when it's dead.
	//public Sprite damagedEnemy;			// An optional sprite of the enemy when it's damaged.

	//public AudioClip[] deathClips;		// An array of audioclips that can play when the enemy dies.
	//public GameObject hundredPointsUI;	// A prefab of 100 that appears when the enemy dies.

	public Rigidbody2D[] projectile;

	private SpriteRenderer ren;			// Reference to the sprite renderer.
	private Transform frontCheck;		// Reference to the position of the gameobject used for checking if something is in front.
	private bool dead = false;			// Whether or not the enemy is dead.
	//private Score score;				// Reference to the Score script.

	private Vector3 rotateVector;
	private float direction;
	private float corrDir = 0f;

	private float lastTime = 0f;

	private GameObject player;

	static Player gamePlayer = null;
	static EnemyManager enemyManager = null;
	static Score score = null;

	void Awake()
	{
		// Setting up the references.
		//ren = transform.Find("body").GetComponent<SpriteRenderer>();
		//frontCheck = transform.Find("frontCheck").transform;
		//score = GameObject.Find("Score").GetComponent<Score>();
		rotateVector = new Vector3 (0, 0, rotateSpeed);

		player = GameObject.FindGameObjectWithTag ("Player");

		lastTime = Time.realtimeSinceStartup + Random.Range(0, fireRate);

		if(enemyManager == null)
			enemyManager = EnemyManager.SharedManager ();

		if (score == null)
			score = Score.SharedScore ();

		if (gamePlayer == null)
			gamePlayer = Player.GetPlayer ();
	}
	
	void FixedUpdate ()
	{

		// Create an array of all the colliders in front of the enemy.
		/*Collider2D[] frontHits = Physics2D.OverlapPointAll(frontCheck.position, 1);
		
		// Check each of the colliders.
		foreach(Collider2D c in frontHits)
		{
			// If any of the colliders is an Obstacle...
			if(c.tag == "Obstacle")
			{
				// ... Flip the enemy and stop checking the other colliders.
				Flip ();
				break;
			}
		}*/
		
		// Set the enemy's velocity to moveSpeed in the x direction.
		//rigidbody2D.velocity = new Vector2(transform.localScale.x * moveSpeed, rigidbody2D.velocity.y);
		if (bEnableRotation) transform.Rotate (rotateVector);


		if (bRangeEnemy) {
			direction = Mathf.Atan2 (transform.position.x - player.transform.position.x,
			                         player.transform.position.y - transform.position.y);
			transform.rotation = Quaternion.Euler (0, 0, (corrDir+direction) * Mathf.Rad2Deg);	//set rotation


			transform.position = new Vector3 (Mathf.Lerp (transform.position.x, player.transform.position.x, 0.01f),
                          Mathf.Lerp (transform.position.y, player.transform.position.y, 0.01f),
                          transform.position.z);

			/*transform.position = new Vector3 (Mathf.Lerp (transform.position.x, player.transform.position.x, 10f*Time.fixedDeltaTime),
                          Mathf.Lerp (transform.position.y, player.transform.position.y, 10f*Time.fixedDeltaTime),
                          transform.position.z);*/

			//transform.position = Vector3.Lerp(transform.position, player.transform.position, Time.fixedDeltaTime);
			//transform.position = Vector3.MoveTowards (transform.position, player.transform.position, 0.1f);

			if(!gamePlayer.IsGameOver) Fire();

		} else {
			direction = Mathf.Atan2 (transform.position.x - player.transform.position.x,
			                         player.transform.position.y - transform.position.y);

			// Cache the horizontal input.
			float xAcc = -Mathf.Sin(direction);
			float yAcc = Mathf.Cos(direction);

			// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
			if(xAcc * rigidbody2D.velocity.x < moveSpeed)
				// ... add a force to the player.
				rigidbody2D.AddForce(Vector2.right * xAcc * moveForce);
			
			if (yAcc * rigidbody2D.velocity.y < moveSpeed)
				rigidbody2D.AddForce (Vector2.up * yAcc * moveForce);
			
			// If the player's horizontal velocity is greater than the maxSpeed...
			if(Mathf.Abs(rigidbody2D.velocity.x) > moveSpeed)
				// ... set the player's velocity to the maxSpeed in the x axis.
				rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * moveSpeed, rigidbody2D.velocity.y);
			
			if(Mathf.Abs(rigidbody2D.velocity.y) > moveSpeed)
				// ... set the player's velocity to the maxSpeed in the x axis.
				rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, Mathf.Sign(rigidbody2D.velocity.y) * moveSpeed);
		}
		// If the enemy has one hit point left and has a damagedEnemy sprite...
		//if(HP == 1 && damagedEnemy != null)
			// ... set the sprite renderer's sprite to be the damagedEnemy sprite.
			//ren.sprite = damagedEnemy;
		
		// If the enemy has zero or fewer hit points and isn't dead yet...
		if(HP <= 0 && !dead)
			// ... call the death function.
			Death ();
	}

	public void Fire()
	{
		if(Time.realtimeSinceStartup - lastTime >= fireRate) {
			int n = Random.Range(0, projectile.Length);
			
			if(!enemyManager.isFullBullet()) {
				Rigidbody2D missileInstance = Instantiate(projectile[n],
				                                          new Vector3(transform.position.x - Mathf.Sin (direction)*0.2f,
				            transform.position.y + Mathf.Cos (direction)*0.2f,
				            transform.position.z),
				                                          Quaternion.Euler (0, 0, direction * Mathf.Rad2Deg)) as Rigidbody2D;
				missileInstance.velocity = new Vector2 (-Mathf.Sin (direction) * missileVelocity,
				                                        Mathf.Cos (direction) * missileVelocity);
				enemyManager.AddBullet();
			}
			
			lastTime = Time.realtimeSinceStartup;
		}
	}

	
	public void Hurt()
	{
		if(gamePlayer.IsGameOver) return;

		// Reduce the number of hit points by one.
		HP--;
	}

	void OnTriggerEnter2D (Collider2D col) 
	{
		// If it hits an enemy...
		if(col.tag == "Player")
		{
			// ... find the Enemy script and call the Hurt function.
			col.gameObject.GetComponent<Player>().Hurt();

			// Destroy the rocket.
			//Destroy (gameObject);
			Hurt();
		}
	}
	
	void Death()
	{
		// Find all of the sprite renderers on this object and it's children.
		SpriteRenderer[] otherRenderers = GetComponentsInChildren<SpriteRenderer>();
		
		// Disable all of them sprite renderers.
		foreach(SpriteRenderer s in otherRenderers)
		{
			s.enabled = false;
		}
		
		// Re-enable the main sprite renderer and set it's sprite to the deadEnemy sprite.
		//ren.enabled = true;
		//ren.sprite = deadEnemy;
		
		// Increase the score by 100 points
		//score.score += 100;
		
		// Set dead to true.
		dead = true;
		
		// Allow the enemy to rotate and spin it by adding a torque.
		//rigidbody2D.fixedAngle = false;
		//rigidbody2D.AddTorque(Random.Range(deathSpinMin,deathSpinMax));
		
		// Find all of the colliders on the gameobject and set them all to be triggers.
		Collider2D[] cols = GetComponents<Collider2D>();
		foreach(Collider2D c in cols)
		{
			c.isTrigger = true;
		}
		
		// Play a random audioclip from the deathClips array.
		//int i = Random.Range(0, deathClips.Length);
		//AudioSource.PlayClipAtPoint(deathClips[i], transform.position);
		
		// Create a vector that is just above the enemy.
		//Vector3 scorePos;
		//scorePos = transform.position;
		//scorePos.y += 1.5f;
		
		// Instantiate the 100 points prefab at this point.
		//Instantiate(hundredPointsUI, scorePos, Quaternion.identity);

		//print ("death");
		Destroy (gameObject);
		score.score += point;
		enemyManager.RemoveEnemy ();
	}
}
