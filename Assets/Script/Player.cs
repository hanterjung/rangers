﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	public int HP = 5;
	public float regenerationTime = 5f;

	private int _hp;
	private float regenTimer = 0f;
	private bool bRegen = false;
	private float hurtEffectDeltaTime = 0f;

	public float hurtEffectTime = 0.5f;
	public GUITexture hurtEffect;
	public GameObject gameOverText;

	private bool bGameOver = false;

	void Awake() {
		instance = this;

		_hp = HP;

		gameOverText.guiText.enabled = false;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		hurtEffectDeltaTime += Time.deltaTime / hurtEffectTime;
		hurtEffect.color = new Color(hurtEffect.color.r, hurtEffect.color.g, hurtEffect.color.b,
		                             Mathf.Lerp((HP-_hp)*24f/255f, 0, hurtEffectDeltaTime));
		//print (hurtEffect.color);

		if (bGameOver) return;

		if (_hp <= 0) Death ();
	}

	void FixedUpdate() {
		if (bGameOver) return;

		print ("hp:" + _hp + " / regen=" + regenTimer);

		if (bRegen) {
			regenTimer += Time.fixedDeltaTime;

			if(regenTimer >= regenerationTime) {
				_hp++;
				regenTimer=0;

				if(_hp == HP) {
					bRegen = false;
				}
			}
		}
	}


	public bool IsGameOver {
		get { return bGameOver; }
	}
	
	public void Hurt() {
		if (bGameOver) return;

		_hp --;
		hurtEffectDeltaTime = 0f;
		bRegen = true;
	}

	void Death() {
		gameOverText.guiText.enabled = true;

		bGameOver = true;

		//Destroy (gameObject);
	}

	private static Player instance = null;
	public static Player GetPlayer() {
		return instance;
	}
}
