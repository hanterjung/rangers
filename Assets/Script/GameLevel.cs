﻿using UnityEngine;
using System.Collections;

public class GameLevel : MonoBehaviour {

	public int level = 1;

	void Awake() {
		instance = this;
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		guiText.text = "LEVEL " + level;
	}

	private static GameLevel instance = null;
	public static GameLevel SharedScore() {
		return instance;
	}
}
