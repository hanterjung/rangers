﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	[HideInInspector]
	private float direction = 0f;			// For determining which way the player is currently facing.

	public Camera mainCamera;
	public Rigidbody2D[] projectile;

	public float moveForce = 365f;			// Amount of force added to move the player left and right.
	public float maxSpeed = 5f;				// The fastest the player can travel in the x axis.
	public float inputForce = 10f;			// Amount of force diff
	
	public int rateOfFire = 10;
	public float missileVelocity = 10f;

	private int mousePressedCount = 0;

	void Awake()
	{
		// Setting up references.
		direction = transform.rotation.z;
	}
	
	
	void Update()
	{
		//setRotation
		//transform.rotation = new Quaternion (0, 0, direction, transform.rotation.w);
		transform.rotation = Quaternion.Euler (0, 0, direction * Mathf.Rad2Deg);	//set rotation

		//print ("tr:" + transform.rotation + " / rdg:" + rigidbody2D.transform.rotation);
		//print ("yposdist="+(mousePos.y - playerPos.y) + " / direction="+direction);
	}
	
	
	void FixedUpdate ()
	{
		// Cache the horizontal input.
		float h = Input.GetAxis("Horizontal") / inputForce;
		float v = Input.GetAxis ("Vertical") / inputForce;

		//print ("h=" + h + " / w=" + w);

		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
		if(h * rigidbody2D.velocity.x < maxSpeed)
			// ... add a force to the player.
			rigidbody2D.AddForce(Vector2.right * h * moveForce);

		if (v * rigidbody2D.velocity.y < maxSpeed)
			rigidbody2D.AddForce (Vector2.up * v * moveForce);
		
		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(rigidbody2D.velocity.x) > maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
			rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);

		if(Mathf.Abs(rigidbody2D.velocity.y) > maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
			rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, Mathf.Sign(rigidbody2D.velocity.y) * maxSpeed);



		//rotation
		Vector3 playerPos3 = mainCamera.WorldToScreenPoint (transform.position);
		Vector2 playerPos = new Vector2 (playerPos3.x, playerPos3.y);
		Vector2 mousePos = Input.mousePosition;
		
		float dist = Vector2.Distance(playerPos, mousePos);
		//direction = Mathf.Acos((mousePos.y-playerPos.y)/dist) * Mathf.Rad2Deg;
		direction = Mathf.Atan2 (playerPos.x - mousePos.x, mousePos.y - playerPos.y);



		//Missile
		if (Input.GetMouseButton(0)) {
			// ... set the animator shoot trigger parameter and play the audioclip.

			if(mousePressedCount == 0) {
				int n = Random.Range(0, projectile.Length);

				Rigidbody2D missileInstance = Instantiate(projectile[n],
				                                          new Vector3(transform.position.x - Mathf.Sin (direction)*0.8f,
				            										  transform.position.y + Mathf.Cos (direction)*0.8f,
				            										  transform.position.z),
				                                          Quaternion.Euler (0, 0, direction * Mathf.Rad2Deg)) as Rigidbody2D;
				missileInstance.velocity = new Vector2 (-Mathf.Sin (direction) * missileVelocity,
				                                        Mathf.Cos (direction) * missileVelocity);
				//missileInstance.velocity = new Vector3 
			}

			mousePressedCount++;
			//print(mousePressedCount);
			if(mousePressedCount == rateOfFire) mousePressedCount = 0;
		}

		if (Input.GetMouseButtonUp(0)) {
			mousePressedCount = 0;
		}
	}
}
