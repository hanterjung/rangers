﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour {
	public int enemyMaxNum = 50;
	public int bulletMaxNum = 200;

	private int enemyCnt = 0;
	private int bulletCnt = 0;

	static private EnemyManager instance = null;

	void Awake() {
		instance = this;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate () {
	}

	public static EnemyManager SharedManager() {
		return instance;
	}

	public void AddEnemy() {
		enemyCnt++;
	}

	public void AddBullet() {
		bulletCnt++;
	}

	public void RemoveEnemy() {
		enemyCnt--;
	}

	public void RemoveBullet() {
		bulletCnt--;
	}

	public bool isFullEnemy() {
		return (enemyCnt >= enemyMaxNum);
	}

	public bool isFullBullet() {
		return (bulletCnt >= bulletMaxNum);
	}

	public int EnemyCount {
		get { return enemyCnt; }
	}

	public int BulletCount {
		get { return bulletCnt; }
	}
}
