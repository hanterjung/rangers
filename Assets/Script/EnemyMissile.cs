﻿using UnityEngine;
using System.Collections;

public class EnemyMissile : MonoBehaviour {
	
	public GameObject explosion;

	static EnemyManager enemyManager = null;
	
	// Use this for initialization
	void Start () {
		Destroy(gameObject, 5);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Awake () {
		enemyManager = EnemyManager.SharedManager ();
	}
	
	
	void OnExplode()
	{
		// Create a quaternion with a random rotation in the z-axis.
		Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
		
		if (explosion == null)
			return;
		
		// Instantiate the explosion where the rocket is with the random rotation.
		GameObject newExplosion = Instantiate(explosion, transform.position, randomRotation) as GameObject;
		float size = Random.Range (0.3f, 0.4f);
		newExplosion.transform.localScale = new Vector3 (size, size, 1);
	}
	
	void OnTriggerEnter2D (Collider2D col) 
	{
		// If it hits an enemy...
		if(col.tag == "Player")
		{
			// ... find the Enemy script and call the Hurt function.
			col.gameObject.GetComponent<Player>().Hurt();
			
			// Call the explosion instantiation.
			OnExplode();
			
			// Destroy the rocket.
			enemyManager.RemoveBullet();
			Destroy (gameObject);
		}
		// Otherwise if it hits a bomb crate...
		else if(col.tag == "Platform")
		{
			// ... find the Bomb script and call the Explode function.
			//col.gameObject.GetComponent<Bomb>().Explode();
			
			OnExplode();
			
			// Destroy the rocket.
			enemyManager.RemoveBullet();
			Destroy (gameObject);
		}
	}
}

